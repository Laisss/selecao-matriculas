from epynet import Network
import numpy as np
import pandas as pd
import math as m
import random

from utils import *
from networkx import draw_networkx
import matplotlib.pyplot as plt
from operator import itemgetter, attrgetter

from sklearn.cluster import KMeans
#from sklearn.svm import SVC 
from sklearn import metrics
#from sklearn.metrics.cluster import fowlkes_mallows_score
from scipy.spatial.distance import cdist
from scipy.spatial import distance

# Simulate the pressures with leak and without leak, in other to calculate sensitivity matrix
# leak_id = 0 - the leak is a emitter value
# leak_id = 1 - the leak is a flow value 

def simulation(inp, leaks=None, sensorIds=None, nodeIds=None, leak_id = None):

    net = Network(inp)
    keys = net.nodes.keys()   # List with uids of each node
    all_nodes = [net.nodes[key] for key in keys]   # List of all possible nodes 
    net.ep.ENclose()
    
    [all_nodes.remove(n) for n in all_nodes if n.node_type != "Junction"]  # Remove elements that are not Junction in all_nodes
    
    # Select the nodes to be used
    if nodeIds is None:    
        nodes_list = all_nodes     
    else:                               
        nodes_list = [n for n in all_nodes if n.uid in nodeIds] 
       
    # Select the sensors to be used
    if sensorIds is None:   
        sensors = all_nodes
    else:
        sensors = [n for n in all_nodes if n.uid in sensorIds]

    pressures = []
    n_nodes = len(nodes_list)
    n_sensors = len(sensors)
    
    if leaks is None:  # Simulation whithout leak
        y_simulated = []
        
        net = Network(inp)
        net.run()         
        
        pressures = [net.nodes[node.uid].pressure.values for node in sensors] # Receive the pressures for each sensor
        net.ep.ENclose()  
       
        if nodeIds is None or len(nodeIds)>1:  # To calculate the sensitivity matrix
            y_simulated = n_nodes * pressures          # Replicates the pressures for each node in the network
            y_simulated = np.asarray(y_simulated).reshape(n_nodes, n_sensors,-1)   # Turns into 3d format        
       
        else:   # To get only the pressures for each node 
            y_simulated = np.asmatrix(pressures)
    
        return y_simulated    
         
    else:      # Simulation with leak    
        for node in nodes_list:
            if leak_id == 1:
                coef_emitter = 0.5
                leaks = leaks % 3.6  # to converte m^3/h to l/s
           
                net = Network(inp)
                net.run()         
                pressure = net.nodes[node.uid].pressure.values # Receive the pressures in the node
                net.ep.ENclose()  
                y_simulated_node = np.asmatrix(pressure)
                y_simulated_node = np.mean(y_simulated_node)
        
                leaks = np.divide(leaks, y_simulated_node ** coef_emitter)
        
            net = Network(inp)
            net.nodes[node.uid].emitter = leaks  # Modify o emitter of one node at time
            net.run()
            
            for node in sensors:  
                pressures.append(net.nodes[node.uid].pressure.values) # Receive the pressures for each sensor for each leak node
            net.ep.ENclose()    
             
        if nodeIds is None or len(nodeIds)>1:           # To calculate the sensitivity matrix
            y_simulated_leak = np.asarray(pressures).reshape(n_nodes,n_sensors,-1) # Turns into 3d format  
        
        else:       # To get the pressures for each node with a node leak
            y_simulated_leak = np.asmatrix(pressures)
    
        return y_simulated_leak
    
    
# Calculate sensitivity matrix using time and emitter
def sensitivity_matrix(inp, leak, leak_id):
    y_simulated = simulation(inp)
    y_simulated_leak = simulation(inp, leaks = leak[0], leak_id = leak_id)

    S = (y_simulated_leak - y_simulated)  
    S = S[:,:,12]
    
    #S = np.split(S, S.shape[2], 2)
    #S = np.asarray(S).transpose()
    #S = S.reshape(S.shape[1],-1)
    
    for i in range(1, len(leak)): 
        y_simulated1 = simulation(inp)
        y_simulated_leak1 = simulation(inp, leak = leak[i], leak_id = leak_id)

        S1 = (y_simulated_leak1 - y_simulated1)/leak[i]
        S1 = S1[:,:,12]
    
        S1 = np.split(S1, S1.shape[2], 2)
        S1 = np.asarray(S1).transpose()
        S1 = S.reshape(S1.shape[1],-1)
    
        S = np.concatenate((S,S1), axis = 1)
        
    S = S.reshape(S.shape[0], -1)
        
    return S

def center_group (inp, y, i_groups):
    net = inp_to_graph(inp)
    node_cord = net.graph['COORDINATES']
    
    node_list = [key for key in y.keys() if y[key] == i_groups]  # List of all nodes in i_groups 
        
    coordinates = [node_cord[node] for node in node_list]    
    coordinates = np.asarray(coordinates).reshape(len(node_list),-1)
        
    kmeans_model = KMeans(init='k-means++', n_clusters = 1, n_init=10).fit(coordinates)
    center = kmeans_model.cluster_centers_
    
    return center

def nodes_next_center(inp, y, i_groups):
    net = inp_to_graph(inp)
    node_cord = net.graph['COORDINATES']

    node_list = [key for key in y.keys() if y[key] == i_groups]  # List of all nodes in i_groups

    coordinates = [node_cord[node] for node in node_list]
    coordinates = np.asarray(coordinates)

    center = center_group(inp, y, i_groups)
    center = np.asarray(center)

    dist = cdist(coordinates, center)

    node_center = [[node_list[i], dist[i,:]] for i in range(len(dist))]
    node_center = dict(node_center)
    node_center = sorted(node_center.items(), key=itemgetter(1))
    
    nodes = []
    for t in node_center:
        node, dist  = t
        nodes.append(node)

    nodes = nodes[:1]
    
    return nodes

# Setorize the network in n_groups, using specifics emitter, sensitivity matrix and kmeans

def grouping(inp, n_groups, leak, leak_id):
    
    S = sensitivity_matrix(inp, leak, leak_id)
    
    kmeans_model = KMeans(init='k-means++', n_clusters = n_groups, n_init=10).fit(S)
    groups = kmeans_model.predict(S)
    error = kmeans_model.score(S)

    net = Network(inp)
    keys = net.nodes.keys()
    all_nodes = [net.nodes[key] for key in keys] # List of all possible nodes
    net.ep.ENclose()
    
    nodes_uid = [n.uid for n in all_nodes if n.node_type == "Junction"] # Remove elements that are not Junction in all_nodes
   
    n_nodes = len(nodes_uid) # Number of nodes in the network
    
    y = [[nodes_uid[indice], groups[indice]] for indice in range(n_nodes)]
    y = dict(y)
    
    #centers = [center_group(inp, y, i_groups) for i_groups in range(n_groups)]
    
    return y

def graph_groups(inp, y, n_groups):

    net = inp_to_graph(inp)
        
    cmap = plt.cm.get_cmap('hsv', n_groups + 1)
    
    for i_groups in range(n_groups):
        
        node_list = [key for key in y.keys() if y[key] == i_groups] # List of all nodes in i_groups
        
        nodes = nodes_next_center(inp, y, i_groups)
        
        # Visualize the network:
        draw_networkx(net, with_labels=False, pos=net.graph['COORDINATES'], node_size=60, nodelist = node_list,
                      node_color = cmap(i_groups))
        
        draw_networkx(net, with_labels=False, pos=net.graph['COORDINATES'], node_size=90, nodelist = nodes,
                      node_color = 'gray')
        
        center = center_group(inp, y, i_groups)
         
        plt.scatter(center[:,0], center[:,1], marker="8", linewidths = 20, color= 'black')
        #plt.annotate('Centroid', (center[:,0], center[:,1]))
            
    fig_size = [0,0]
    fig_size[0] = 11
    fig_size[1] = 14
    plt.rcParams["figure.figsize"] = fig_size
    #plt.legend( loc='lower left', fontsize=8)
    fig_size
    plt.savefig(str(inp)+ str(n_groups) + 'groups.png')

    plt.show()
    
