import networkx as nx
import re


import json

from collections import namedtuple


def load_configurations(configuration_file):
	print(configuration_file)
	with open(configuration_file, 'r') as content_file:
		content = content_file.read()

	return json.loads(content, object_hook=lambda d: namedtuple('Configuration', d.keys())(*d.values()))


def find_nodes(g, comparison_func):
    """
    Function to find specific nodes due to comparison function.
    example:
    junctions = NetworkDataModel.find_nodes(lambda x: (x['TYPE'] == 'JUNCTION'))
    """
    matched_nodes = []
    for n in g.nodes_iter():
        node = g.node[n]
        if comparison_func(node):
            matched_nodes.append(n)
    return matched_nodes


def find_edges(g, comparison_func):
    """
    Function to find specific edges due to comparison function.
    example:
    valves = NetworkDataModel.find_edges(lambda x: (x['TYPE'] == 'TCV') and float(x['SETTING']) > 1)
    """
    matched_edges = []
    for n1, n2 in g.edges_iter():
        edge = g.edge[n1][n2]
        if comparison_func(edge):
            matched_edges.append((n1, n2))
    return matched_edges


def change_nodes_attr(g, nodes, attrs_dict):
    """
    Procedure to change nodes attributes due to attributes dictionary
    example:
    NetworkDataModel.change_nodes_attr(junctions, {'PATTERN': 'demand_pattern'})
    """
    for n in nodes:
        for attr in attrs_dict:
            value = attrs_dict[attr]
            g.node[n][attr] = value


def change_graph_attr(g, attrs_dict):
    """
    Procedure to change graph attributes due to attributes dictionary
    example:
    NetworkDataModel.change_graph_attr({'TITLE': 'New Title'})
    """
    for attr in attrs_dict:
        value = attrs_dict[attr]
        g.graph[attr] = value


def change_edges_attr(g, edges, attrs_dict):
    """
    Procedure to change nodes attributes due to attributes dictionary
    example:
    NetworkDataModel.change_edges_attr(edges, {'ROUGHNESS': '10'})
    """
    for n1, n2 in edges:
        for attr in attrs_dict:
            value = attrs_dict[attr]
            g.edge[n1][n2][attr] = value


def remove_unconnected_nodes(g):
    """
    This procedure removes all unconnected nodes from graph.
    """
    nodes_to_remove = []
    for n in g.nodes_iter():
        neighbors = g.neighbors(n)
        if len(neighbors) == 0:
            nodes_to_remove.append(n)
    g.remove_nodes_from(nodes_to_remove)

def inp_to_graph(inp_file):
    """
    Function to import inp. file into directed networkx graph structure.
    @param inp_file: path to inp. file.
    @return: directed networkx graph object.
    """
    inp_file = inp_file
    # ==================================================================================================================
    # Initial Module:

    # Graph initialization
    G = nx.DiGraph()
    G.graph['TITLE'] = ''
    G.graph['PATTERNS'] = {}
    G.graph['CURVES'] = {}
    G.graph['CONTROLS'] = ''
    G.graph['RULES'] = ''
    G.graph['ENERGY'] = ''
    G.graph['REACTIONS'] = ''
    G.graph['TIMES'] = {}
    G.graph['REPORT'] = ''
    G.graph['OPTIONS'] = {}
    G.graph['COORDINATES'] = {}
    G.graph['VERTICES'] = {}
    G.graph['LABELS'] = {}
    G.graph['BACKDROP'] = ''

    times_parameters = ['duration', 'hydraulic timestep', 'quality timestep', 'pattern timestep', 'pattern start',
                    'report timestep', 'report start', 'start clocktime', 'statistic']

    options_parameters = ['units', 'headloss', 'hydraulic', 'quality', 'viscosity', 'diffusivity', 'specific gravity',
                      'trials', 'accuracy', 'unbalanced', 'pattern', 'demand multiplier', 'emitter exponent',
                      'tolerance', 'map']

    # Each line processing
    section = ''
    with open(inp_file) as f:
	    for line in f:

		# ==========================================================================================================
		# Preparing Module:

		# Get section name from line
		if re.match('\[([A-Z]+)\]', line):
		    section = re.sub('\W', '', line)
		    continue

		# Pass an empty line and a comment line
		if len(line.strip()) == 0 or line.strip()[0] == ';':
		    continue

		# Remove comments from line
		line = re.sub(';.*', '', line)

		# ==========================================================================================================
		# Processing Module:

		# [TITLE]:
		if section == 'TITLE':
		    G.graph['TITLE'] += line

		# [JUNCTIONS]:
		if section == 'JUNCTIONS':
		    row = line.split()
		    type = 'JUNCTION'
		    node = row[0]
		    elevation = float(row[1])
		    basedemand = 0.0
		    pattern = ''
		    if len(row) > 2:
		        basedemand = float(row[2])
		    if len(row) > 3:
		        pattern = row[3]
		    G.add_node(n=node,
		               attr_dict={'ELEVATION': elevation,
		                          'BASEDEMAND': basedemand,
		                          'PATTERN': pattern,
		                          'TYPE': type})

		# [RESERVOIRS]:
		if section == 'RESERVOIRS':
		    row = line.split()
		    type = 'RESERVOIR'
		    node = row[0]
		    head = float(row[1])
		    pattern = ''
		    if len(row) > 2:
		        pattern = row[2]
		    G.add_node(n=node,
		               attr_dict={'HEAD': head,
		                          'PATTERN': pattern,
		                          'TYPE': type})

		# [TANKS]:
		if section == 'TANKS':
		    row = line.split()
		    type = 'TANK'
		    node = row[0]
		    elev = float(row[1])
		    initlvl = float(row[2])
		    minlvl = float(row[3])
		    maxlvl = float(row[4])
		    diam = float(row[5])
		    minvol = float(row[6])
		    pattern = ''
		    if len(row) > 7:
		        pattern = float(row[7])
		    G.add_node(n=node,
		               attr_dict={'ELEVATION': elev,
		                          'INIT_LEVEL': initlvl,
		                          'MIN_LEVEL': minlvl,
		                          'MAX_LEVEL': maxlvl,
		                          'DIAMETER': diam,
		                          'MIN_VOLUME': minvol,
		                          'CURVE': pattern,
		                          'TYPE': type})

		# [PIPES]:
		if section == 'PIPES':
		    row = line.split()
		    type = 'PIPE'
		    link = row[0]
		    fromnode = row[1]
		    tonode = row[2]
		    length = float(row[3])
		    diam = float(row[4])
		    roughness = float(row[5])
		    minloss = '0'
		    status = 'Open;'
		    G.add_edge(fromnode, tonode,
		               attr_dict={'LENGTH': length,
		                          'DIAMETER': diam,
		                          'ROUGHNESS': roughness,
		                          'MINOR_LOSS': minloss,
		                          'STATUS': status,
		                          'ID': link,
		                          'TYPE': type})

		# [PUMPS]:
		if section == 'PUMPS':
		    row = line.split()
		    type = 'PUMP'
		    link = row[0]
		    fromnode = row[1]
		    tonode = row[2]
		    params = ' '.join(row[3:])
		    G.add_edge(fromnode, tonode,
		               attr_dict={'PARAMETERS': params,
		                          'ID': link,
		                          'TYPE': type})

		# [VALVES]:
		if section == 'VALVES':
		    row = line.split()
		    type = row[4]
		    link = row[0]
		    fromnode = row[1]
		    tonode = row[2]
		    diam = float(row[3])
		    setting = row[5]
		    minloss = '0'
		    G.add_edge(fromnode, tonode,
		               attr_dict={'DIAMETER': diam,
		                          'ID': link,
		                          'TYPE': type,
		                          'SETTING': setting,
		                          'MINOR_LOSS': minloss})

		# [TAGS]:
		if section == 'TAGS':
		    row = line.split()
		    tag = row[2]
		    if row[0].lower() == 'node':
		        node = row[1]
		        G.node[node]['TAG'] = tag
		    if row[0].lower() == 'link':
		        link_id = row[1]
		        for n1, n2 in G.edges():
		            if G.edge[n1][n2]['ID'] == link_id:
		                G.edge[n1][n2]['TAG'] = tag

		# [DEMANDS]:
		if section == 'DEMANDS':
		    pattern = ''
		    row = line.split()
		    node = row[0]
		    basedemand = float(row[1])
		    if len(row) == 3:
		        pattern = row[2]
		    G.node[node]['PATTERN'] = pattern
		    G.node[node]['BASEDEMAND'] = basedemand

		# [STATUS]:
		if section == 'STATUS':
		    row = line.split()
		    link_id = row[0]
		    status = row[1]
		    for n1, n2 in G.edges():
		        if G.edge[n1][n2]['ID'] == link_id:
		            G.edge[n1][n2]['INIT_STATUS'] = status


		# [PATTERNS]:
		if section == 'PATTERNS':
		    row = line.split()
		    pattern = row[0]
		    values = [float(i) for i in row[1:]]
		    if pattern not in G.graph['PATTERNS']:
		        G.graph['PATTERNS'][pattern] = values
		    else:
		        G.graph['PATTERNS'][pattern] += values

		# [CURVES]:
		if section == 'CURVES':
		    row = line.split()
		    pattern = row[0]
		    values = [tuple(float(i) for i in row[1:])]
		    if pattern not in G.graph['CURVES']:
		        G.graph['CURVES'][pattern] = values
		    else:
		        G.graph['CURVES'][pattern] += values

		# [CONTROLS]:
		if section == 'CONTROLS':
		    G.graph['CONTROLS'] += line

		# [RULES]:
		if section == 'RULES':
		    G.graph['RULES'] += line

		# [ENERGY]:
		if section == 'ENERGY':
		    G.graph['ENERGY'] += line

		# [EMITTERS]:
		if section == 'EMITTERS':
		    row = line.split()
		    node = row[0]
		    emitter = float(row[1])
		    G.node[node]['EMITTER'] = emitter

		# [QUALITY]:
		if section == 'QUALITY':
		    row = line.split()
		    node = row[0]
		    quality = float(row[1])
		    G.node[node]['QUALITY'] = quality

		# [SOURCES]:
		if section == 'SOURCES':
		    row = line.split()
		    node = row[0]
		    type = row[1]
		    strength = float(row[2])
		    if len(row) == 4:
		        pattern = row[3]
		        G.node[node]['PATTERN'] = pattern
		    G.node[node]['SOURCE_TYPE'] = type
		    G.node[node]['SOURCE_STRENGTH'] = strength

		# [REACTIONS]:
		if section == 'REACTIONS':
		    G.graph['REACTIONS'] += line

		# [MIXING]:
		if section == 'MIXING':
		    row = line.split()
		    node = row[0]
		    model = ' '.join(row[1:])
		    G.node[node]['MIXING_MODEL'] = model

		# [TIMES]:
		if section == 'TIMES':
		    line = line.lower()
		    for times_parameter in times_parameters:
		        if re.findall(times_parameter, line):
		            value = re.sub(times_parameter, '', line).strip()
		            G.graph['TIMES'][times_parameter] = value

		# [REPORT]:
		if section == 'REPORT':
		    G.graph['REPORT'] += line

		# [OPTIONS]:
		if section == 'OPTIONS':
		    for options_parameter in options_parameters:
		        if re.search(options_parameter, line, re.IGNORECASE):
		            value = re.sub(options_parameter, '', line, flags=re.IGNORECASE).strip()
		            G.graph['OPTIONS'][options_parameter] = value

		# [COORDINATES]:
		if section == 'COORDINATES':
		    row = line.split()
		    node = row[0]
		    x = float(re.sub(',', '.', row[1]))
		    y = float(re.sub(',', '.', row[2]))
		    G.graph['COORDINATES'][node] = (x, y)

		# [VERTICES]:
		if section == 'VERTICES':
		    row = line.split()
		    pattern = row[0]
		    values = [tuple(float(re.sub(',', '.', i)) for i in row[1:])]
		    if pattern not in G.graph['VERTICES']:
		        G.graph['VERTICES'][pattern] = values
		    else:
		        G.graph['VERTICES'][pattern] += values

		# [LABELS]:
		if section == 'LABELS':
		    row = line.split()
		    node = row[2]
		    x = float(row[0])
		    y = float(row[1])
		    G.graph['LABELS'][node] = (x, y)

		# [BACKDROP]:
		if section == 'BACKDROP':
		    G.graph['BACKDROP'] += line

    return G

def graph_to_inp(inp_file, G, ignore_section=None):
    """
    Function to export networkx graph into inp. file.
    @param inp_file: path to inp. file.
    @param ignore_section: sections of the inp. file, which will not be written.
    @param G: networkx graph object.
    @return: write new inp. file.
    """
    if ignore_section is None:
        ignore_section = []

    with open(inp_file, 'w') as f:
        # Write nodes in tmp strings:
        junctions = ''
        reservoirs = ''
        tanks = ''
        demands = ''
        tags = ''
        emitters = ''
        quality = ''
        sources = ''
        mixing = ''
        for n in G.nodes_iter():
            node = G.node[n]
            if node['TYPE'] == 'JUNCTION':
                junctions += (' ' * 10).join([n, str(node['ELEVATION']), '\n'])
                pattern = node['PATTERN']
                demand = node['BASEDEMAND']

                demands += (' ' * 10).join([n, str(demand), str(pattern), '\n'])

            elif node['TYPE'] == 'RESERVOIR':
                reservoirs += (' ' * 10).join([n, str(node['HEAD']), str(node['PATTERN']), '\n'])

            elif node['TYPE'] == 'TANK':
                tanks += (' ' * 10).join([n, str(node['ELEVATION']), str(node['INIT_LEVEL']),
                                          str(node['MIN_LEVEL']), str(node['MAX_LEVEL']),
                                          str(node['DIAMETER']), str(node['MIN_VOLUME']),
                                          str(node['CURVE']), '\n'])
            if 'TAG' in node:
                tags += (' ' * 10).join(['NODE', n, str(node['TAG']), '\n'])

            if 'EMITTER' in node:
                emitters += (' ' * 10).join([n, str(node['EMITTER']), '\n'])

            if 'QUALITY' in node:
                quality += (' ' * 10).join([n, str(node['QUALITY']), '\n'])

            if 'SOURCE_TYPE' in node:
                sources += (' ' * 10).join([n, str(node['SOURCE_TYPE']), str(node['SOURCE_STRENGTH']),
                                            str(node['PATTERN']), '\n'])

            if 'MIXING_MODEL' in node:
                mixing += (' ' * 10).join([n, str(node['MIXING_MODEL']), '\n'])

        # Write edges in tmp strings:
        pipes = ''
        pumps = ''
        valves = ''
        status = ''

        for n1, n2 in G.edges_iter():
            edge = G.edge[n1][n2]
            if edge['TYPE'] == 'PIPE':
                pipes += (' ' * 10).join([edge['ID'], n1, n2,
                                          str(edge['LENGTH']), str(edge['DIAMETER']),
                                          str(edge['ROUGHNESS']), str(edge['MINOR_LOSS']),
                                          str(edge['STATUS']), '\n'])
            if edge['TYPE'] == 'PUMP':
                pumps += (' ' * 10).join([edge['ID'], n1, n2, str(edge['PARAMETERS']), '\n'])

            if edge['TYPE'] == 'VALVE':
                pumps += (' ' * 10).join([edge['ID'], n1, n2, str(edge['DIAMETER']),
                                          str(edge['TYPE']), str(edge['SETTING']),
                                          str(edge['MINOR_LOSS']), '\n'])

            if 'TAG' in edge:
                tags += (' ' * 10).join(['LINK', edge['ID'], str(edge['TAG']), '\n'])

            if 'INIT_STATUS' in edge:
                status += (' ' * 10).join([edge['ID'], str(edge['INIT_STATUS']), '\n'])

        # Write patterns in tmp string:
        patterns = ''
        for p in G.graph['PATTERNS']:
            pattern = G.graph['PATTERNS'][p]
            values = []
            for value in pattern:
                values.append(value)
                if len(values) == 6:
                    values = [str(i) for i in values]
                    patterns += p + (' ' * 10) + (' ' * 10).join(values) + '\n'
                    values = []

        # Write times in tmp string
        times = ''
        for t in G.graph['TIMES']:
            value = G.graph['TIMES'][t]
            times += t + (' ' * 10) + value + '\n'

        # Write options in tmp string
        options = ''
        for o in G.graph['OPTIONS']:
            value = G.graph['OPTIONS'][o]
            options += o + (' ' * 10) + str(value) + '\n'

        # Write curves in tmp string:
        curves = ''
        for c in G.graph['CURVES']:
            curve = G.graph['CURVES'][c]
            for values in curve:
                values = [str(i) for i in values]
                curves += c + (' ' * 10) + (' ' * 10).join(values) + '\n'

        # Write coordinates in tmp string:
        coordinates = ''
        for c in G.graph['COORDINATES']:
            coord = G.graph['COORDINATES'][c]
            coord = [str(i) for i in coord]
            coordinates += c + (' ' * 10) + (' ' * 10).join(coord) + '\n'

        # Write vertices in tmp string:
        vertices = ''
        for v in G.graph['VERTICES']:
            vertex = G.graph['VERTICES'][v]
            for coord in vertex:
                coord = [str(i) for i in coord]
                vertices += v + (' ' * 10) + (' ' * 10).join(coord) + '\n'

        # Write coordinates in tmp string:
        labels = ''
        for c in G.graph['LABELS']:
            labels += (' ' * 10).join([str(G.graph['LABELS'][c][0]),
                                       str(G.graph['LABELS'][c][1]),
                                       c, '\n'])

        section = 'TITLE'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')

        section = 'JUNCTIONS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(junctions)
            f.write('\n')

        section = 'RESERVOIRS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(reservoirs)
            f.write('\n')

        section = 'TANKS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(tanks)
            f.write('\n')

        section = 'PIPES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(pipes)
            f.write('\n')

        section = 'PUMPS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(pumps)
            f.write('\n')

        section = 'VALVES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(valves)
            f.write('\n')

        section = 'TAGS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(tags)
            f.write('\n')

        section = 'DEMANDS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(demands)
            f.write('\n')

        section = 'STATUS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(status)
            f.write('\n')

        section = 'PATTERNS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(patterns)
            f.write('\n')

        section = 'CURVES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(curves)
            f.write('\n')

        section = 'CONTROLS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')

        section = 'RULES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')

        section = 'ENERGY'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')

        section = 'EMITTERS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(emitters)
            f.write('\n')

        section = 'QUALITY'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(quality)
            f.write('\n')

        section = 'SOURCES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(sources)
            f.write('\n')

        section = 'REACTIONS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')

        section = 'MIXING'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(mixing)
            f.write('\n')

        section = 'TIMES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(times)
            f.write('\n')

        section = 'REPORT'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')

        section = 'OPTIONS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(options)
            f.write('\n')

        section = 'COORDINATES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(coordinates)
            f.write('\n')

        section = 'VERTICES'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(vertices)
            f.write('\n')

        section = 'LABELS'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(labels)
            f.write('\n')

        section = 'BACKDROP'
        if section not in ignore_section:
            f.write('[{}]\n'.format(section))
            f.write(G.graph[section])
            f.write('\n')


def remove_leaves(g, equal_attrs_dict, merging_funcs_dict):
    """
    This procedure remove leaves from graph.
    equal_attrs_dict: dictionary with nodes attributes and attributes values, which must be equal to merge
                        neighbor and leave.
    merging_funcs_dict: dictionary with nodes attributes, which must be merged, and values,
                        which are rules(functions), to merge attributes
    """

    finished = False

    while not finished:
        nodes_to_remove = []
        for n in g.nodes_iter():
            neighbors = g.neighbors(n)
            if len(neighbors) == 1:

                can_be_merged = True
                neighbor = g.node[neighbors[0]]
                node = g.node[n]

                for attr in equal_attrs_dict.keys():
                    if (attr not in node) or (attr not in neighbor) or (neighbor[attr] != node[attr]):
                        can_be_merged = False

                if can_be_merged:
                    for attr in merging_funcs_dict:
                        func = merging_funcs_dict[attr]
                        neighbor[attr] = func(neighbor[attr], node[attr])
                    nodes_to_remove.append(n)

        if len(nodes_to_remove) == 0:
            finished = True
        else:
            g.remove_nodes_from(nodes_to_remove)


def set_middle_node(g,
                    new_node_dict,
                    target_edge_tuple,
                    attrs_change_functions_edge_1,
                    attrs_change_functions_edge_2):
    """
    This procedure sets the middle point on the specific edge. This edge will be replaced with two others edges.
    :param new_node_dict: Dictionary with new node parameters.
    :param target_edge_tuple: A tuple with target edge.
    :param attrs_change_functions_edge_1: A dictionary with functions, which will be applied to the original edge
    attributes.
    :param attrs_change_functions_edge_2: A dictionary with functions, which will be applied to the original edge
    attributes.

    example:
    import networkx as nx
    import matplotlib.pyplot as plt

    G = nx.Graph()
    G.add_node('node1', {'demand': 100, 'high': 10, 'type': '1'})
    G.add_node('node2', {'demand': 70, 'high': 30, 'type': '1'})
    G.add_node('node3', {'demand': 30, 'high': 20, 'type': '1'})
    G.add_node('node4', {'demand': 200, 'high': 80, 'type': '1'})
    G.add_node('node5', {'demand': 160, 'high': 80, 'type': '2'})
    G.add_edge('node1', 'node2', {'length': 1000, 'id': '1'})
    G.add_edge('node2', 'node3', {'length': 1000, 'id': '1'})
    G.add_edge('node3', 'node1', {'length': 1000, 'id': '1'})
    G.add_edge('node1', 'node4', {'length': 1000, 'id': '1'})
    G.add_edge('node2', 'node5', {'length': 1000, 'id': '1'})
    G.graph['COORDINATES'] = {'node1': (3, 2), 'node2': (4, 5), 'node3': (7, 5), 'node4': (7, 2), 'node5': (2, 7)}

    nx.draw(G, node_size=25, pos=G.graph['COORDINATES'])
    plt.show()

    N = NetworkDataModel()
    N.set_network(G)

    new_node_dict = {'demand': 0, 'high': 20, 'type': 3}
    target_edge_tuple = ('node1', 'node2')
    attrs_change_functions_edge_1 = {'length': lambda x: x / 2, 'id': lambda x: x + '_first'}
    attrs_change_functions_edge_2 = {'length': lambda x: x / 2, 'id': lambda x: x + '_second'}
    N.set_middle_node(new_node_dict, target_edge_tuple, attrs_change_functions_edge_1, attrs_change_functions_edge_2)

    new_x = (N.Network.graph['COORDINATES']['node1'][0] + N.Network.graph['COORDINATES']['node2'][0]) / 2
    new_y = (N.Network.graph['COORDINATES']['node1'][1] + N.Network.graph['COORDINATES']['node2'][1]) / 2
    N.Network.graph['COORDINATES']['node1|node2'] = (new_x, new_y)

    nx.draw(N.Network, node_size=25, pos=N.Network.graph['COORDINATES'])
    plt.show()
    """

    old_edge_dict = g.edge[target_edge_tuple[0]][target_edge_tuple[1]].copy()
    g.remove_edge(target_edge_tuple[0], target_edge_tuple[1])

    new_node_name = target_edge_tuple[0] + '_' + target_edge_tuple[1]
    g.add_node(new_node_name, new_node_dict)

    new_dict_1 = old_edge_dict.copy()
    for attr_to_change in attrs_change_functions_edge_1.keys():
        func = attrs_change_functions_edge_1[attr_to_change]
        new_dict_1[attr_to_change] = func(new_dict_1[attr_to_change])

    new_dict_2 = old_edge_dict.copy()
    for attr_to_change in attrs_change_functions_edge_2.keys():
        func = attrs_change_functions_edge_2[attr_to_change]
        new_dict_2[attr_to_change] = func(new_dict_2[attr_to_change])

    g.add_edge(target_edge_tuple[0], new_node_name, new_dict_1)
    g.add_edge(target_edge_tuple[1], new_node_name, new_dict_2)


def import_network_from_data_frames(nodes_df, edges_df, connections_df):
    """
    @param nodes_df:
    @param edges_df:
    @param connections_df:
    @return:
    """
    g = nx.Graph()

    g.add_nodes_from(nodes_df.index.tolist())
    for node in g.nodes_iter():
        attrs = nodes_df.loc[node].to_dict()
        g.node[node] = attrs

    for row in connections_df.iterrows():
        id_ = row[0]
        attrs = edges_df.loc[id_].to_dict()
        attrs['ID'] = id_
        edge = tuple(row[1])
        g.add_edge(edge[0], edge[1], attrs)

    return g
